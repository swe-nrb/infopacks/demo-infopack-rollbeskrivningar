# Demo Informationspaket - Rollbeskrivningar  

Detta informationpaket är ett exempel på hur ett textdokument på ett intelligent
sätt kan underhållas och distribueras. Det är en delleverans från projektet
"Nationella Riktlinjer BIM".  

Målet med paketet är i första hand att illustrera hur ett dokument kan underhållas
och distribueras och innehållet kan därför vara bristfälligt.

## Förvaltare  

Vill du komma i kontakt med oss? Följ länkarna nedan.

* [Johan Asplund](https://www.linkedin.com/in/bimjohan/)
* [Sara Beltrami](https://www.linkedin.com/in/sara-beltrami-64678a3/)
* [Håkan Norberg](https://www.linkedin.com/in/h%C3%A5kan-norberg-2322301a/)

## Paketinformation  

Paketet innehåller ett presteg för att generera dokumentet.  

### Byggscript

För att bygga ihop paketet:

`npm run-script prebuild`

## Förbättringsförslag  

Har du synpunkter på innehållet i paketet, lämna ett [förbättringsförslag](https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar/issues)

## Publicerat material  

Nedan följer länkar till det senaste publicerade materialet

### Läs online  

https://swe-nrb.gitlab.io/demo-infopack-rollbeskrivningar/

### Ladda ned

https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar/builds/artifacts/master/download?job=build_deliverables

### Använd data i din egen app

Det är fullt möjligt att använda data direkt ifrån källan till din egen app. Länk:  

https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar/raw/master/data.json

Hälsningar NRB
